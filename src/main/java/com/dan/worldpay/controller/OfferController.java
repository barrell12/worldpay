package com.dan.worldpay.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dan.worldpay.model.Offer;
import com.dan.worldpay.service.OfferService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OfferController {

  private final OfferService offerService;

  @PostMapping("/offer")
  public String postOffer(@RequestBody Offer offer) {
    log.info("Received offer to be submitted to the system, offer={}", offer);
    return offerService.submitOffer(offer);
  }

  @DeleteMapping("/offer/{id}/expire")
  public void expireOffer(@PathVariable(value = "id") String id) {
    if (!id.isEmpty()) {
      log.info("Received request to expire offer id={}", id);
      offerService.expireOffer(id);
    }
  }

  @DeleteMapping("/offer/{id}")
  public void cancelOffer(@PathVariable(value = "id") String id) {
    if (!id.isEmpty()) {
      log.info("Received request to cancel offer id={}", id);
      offerService.cancelOffer(id);
    }
  }

  @GetMapping("/offer/{id}")
  public Offer getOffer(@PathVariable(value = "id") String id) {
    log.info("Received request to retrieve offer id={} from the system", id);
    return offerService.retrieveOffer(id);
  }

  @GetMapping("/offers")
  public List<Offer> getOffers() {
    log.info("Received request to retrieve all valid offers from the system");
    return offerService.retrieveValidOffers();
  }

  @GetMapping("/search")
  public List<Offer> searchOffers(@RequestParam(value = "keyword", required = true) String keyword) {
    log.info("Received request to search offers for keyword={}", keyword);
    return offerService.searchOffers(keyword);
  }
}
