package com.dan.worldpay.service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.dan.worldpay.exception.OfferNotFoundException;
import com.dan.worldpay.model.Offer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OfferService {

  private final TimeService timeService;
  private final HashMap<String, Offer> offers;

  public OfferService(TimeService timeService) {
    this.timeService = Objects.requireNonNull(timeService);
    offers = new HashMap<>();
  }

  /**
   * Submits an offer to the system and starts an expired offer garbage collection.
   *
   * @param offer the offer to be submitted to the system, must not be null
   * @return the unique identifier of the successfully submitted offer
   */
  public String submitOffer(Offer offer) {
    Objects.requireNonNull(offer);

    String id = UUID.randomUUID().toString();
    offers.put(id, offer);

    log.info("Successfully submitted offer to system with id={}", id);
    return id;
  }

  /**
   * Attempts to retrieve the offer from the system. If the id is not found or the offer is expired, an exception is
   * thrown.
   *
   * @param id the unique identifier of the offer to retrieve
   * @return the requested offer
   */
  public Offer retrieveOffer(String id) {
    Offer offer = Optional.ofNullable(offers.get(id))
        .orElseThrow(() -> OfferNotFoundException.idNotFound(id));

    log.info("Offer id={} successfully found in system", id);

    if (isOfferExpired(offer)) {
      throw OfferNotFoundException.idExpired(id);
    }

    log.info("Offer id={} successfully retrieved", id);
    return offer;
  }

  /**
   * Retrieves all non-expired offers from the system.
   *
   * @return all non-expired offers
   */
  public List<Offer> retrieveValidOffers() {
    return offers.values().stream()
        .filter(offer -> !isOfferExpired(offer))
        .collect(Collectors.toList());
  }

  /**
   * Cancels an offer from the system.
   *
   * @param id the unique identifier of the offer to cancel
   */
  public void cancelOffer(String id) {
    // Check if present
    retrieveOffer(id);

    offers.remove(id);
    log.info("Successfully cancelled offer id={}", id);
  }

  /**
   * Expires an offer on the system.
   *
   * @param id the unique identifier of the offer to expire
   */
  public void expireOffer(String id) {
    Offer offer = retrieveOffer(id);
    offers.put(id, offer.withExpirationTime(timeService.getCurrentTime() + 1L));
    log.info("Successfully expired offer id={}", id);
  }

  /**
   * Aggregates and returns offers that have descriptions containing the provided keyword.
   *
   * @param keyword the string to find in offer descriptions
   * @return a list of offers that have a description containing the provided keyword
   */
  public List<Offer> searchOffers(String keyword) {
    List<Offer> matchingOffers = offers.values().stream()
        .filter(offer -> offer.getDescription().contains(keyword))
        .collect(Collectors.toList());

    log.info("Successfully found {} offers with a description containing keyword={}", matchingOffers.size(), keyword);
    return matchingOffers;
  }

  private boolean isOfferExpired(Offer offer) {
    return (offer.getExpirationTime() > timeService.getCurrentTime());
  }
}
