package com.dan.worldpay.service;

import org.springframework.stereotype.Component;

@Component
public class TimeService {

  /**
   * Returns the current system time in millis.
   *
   * @return the current system time in millis
   */
  public long getCurrentTime() {
    return System.currentTimeMillis();
  }
}
