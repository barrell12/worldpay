package com.dan.worldpay.model;

/**
 * Enum representing available currencies to post on the offer market.
 */
public enum Currency {
  GBP, USD, EUR
}
