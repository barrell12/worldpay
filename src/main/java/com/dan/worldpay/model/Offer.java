package com.dan.worldpay.model;

import java.math.BigInteger;

import lombok.NonNull;
import lombok.Value;
import lombok.With;

/**
 * Represents an offer in the system.
 */
@Value
public class Offer {

  @With
  private final long expirationTime;
  @NonNull
  private final Currency currency;
  @NonNull
  private final BigInteger value;
  // Debatable that the description could have NotEmpty validation
  @NonNull
  private final String description;
}
