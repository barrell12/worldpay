package com.dan.worldpay.exception;

public class OfferNotFoundException extends RuntimeException {

  public OfferNotFoundException(String message) {
    super(message);
  }

  public static OfferNotFoundException idNotFound(String id) {
    return new OfferNotFoundException(String.format("Offer id=%s not found", id));
  }

  public static OfferNotFoundException idExpired(String id) {
    return new OfferNotFoundException(String.format("Offer id=%s is expired", id));
  }
}
