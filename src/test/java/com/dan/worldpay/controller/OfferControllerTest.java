package com.dan.worldpay.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.dan.worldpay.model.Currency;
import com.dan.worldpay.model.Offer;
import com.dan.worldpay.service.OfferService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Extremely simple test for HTTP compatibility.
 * In reality this would cover all test cases.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(OfferController.class)
public class OfferControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private OfferService offerService;

  @Test
  public void canSubmitOffer() throws Exception {
    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ONE, "foo");

    ObjectMapper objectMapper = new ObjectMapper();

    String serialised = objectMapper.writeValueAsString(offer);
    mockMvc.perform(post("/offer")
        .header("Content-Type", "application/json")
        .content(serialised))
        .andExpect(status().isOk());

    verify(offerService, times(1)).submitOffer(offer);
  }
}
