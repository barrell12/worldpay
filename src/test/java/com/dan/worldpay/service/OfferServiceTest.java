package com.dan.worldpay.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.dan.worldpay.exception.OfferNotFoundException;
import com.dan.worldpay.model.Currency;
import com.dan.worldpay.model.Offer;

class OfferServiceTest {

  private OfferService offerService;
  private TimeService mockTimeService;

  @BeforeEach
  public void setUp() {
    mockTimeService = Mockito.mock(TimeService.class);
    this.offerService = new OfferService(mockTimeService);
  }

  @Test
  public void simpleSubmitAndRetrieve() {
    when(mockTimeService.getCurrentTime()).thenReturn(1L);

    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ONE, "foo");

    String id = offerService.submitOffer(offer);

    Offer retrievedOffer = offerService.retrieveOffer(id);

    assertEquals(offer, retrievedOffer);
  }

  @Test
  public void nullSubmit() {
    assertThrows(NullPointerException.class, () -> offerService.submitOffer(null));
  }

  @Test
  public void expiredRetrieve() {
    when(mockTimeService.getCurrentTime()).thenReturn(0L);

    Offer offer = new Offer(1L, Currency.GBP, BigInteger.ONE, "foo");

    String id = offerService.submitOffer(offer);

    assertThrows(OfferNotFoundException.class, () -> offerService.retrieveOffer(id));
  }

  @Test
  public void notFoundRetrieve() {
    assertThrows(OfferNotFoundException.class, () -> offerService.retrieveOffer("foo"));
  }

  @Test
  public void retrievesValidOffers() {
    when(mockTimeService.getCurrentTime()).thenReturn(1L);

    Offer offer1 = new Offer(0L, Currency.GBP, BigInteger.ONE, "foo");
    Offer offer2 = new Offer(0L, Currency.GBP, BigInteger.ONE, "foo");
    Offer offer3 = new Offer(2L, Currency.GBP, BigInteger.ONE, "foo");

    offerService.submitOffer(offer1);
    offerService.submitOffer(offer2);
    offerService.submitOffer(offer3);

    List<Offer> validOffers = offerService.retrieveValidOffers();

    assertThat(validOffers, hasSize(2));
    assertThat(validOffers, containsInAnyOrder(offer1, offer2));
  }

  @Test
  public void cancelOffer() {
    when(mockTimeService.getCurrentTime()).thenReturn(1L);

    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ONE, "foo");

    String id = offerService.submitOffer(offer);

    offerService.retrieveOffer(id);

    offerService.cancelOffer(id);

    assertThrows(OfferNotFoundException.class, () -> offerService.retrieveOffer(id));
  }

  @Test
  public void cantCancelExpired() {
    when(mockTimeService.getCurrentTime()).thenReturn(2L);

    Offer offer = new Offer(1L, Currency.GBP, BigInteger.ONE, "foo");

    String id = offerService.submitOffer(offer);

    offerService.retrieveOffer(id);

    when(mockTimeService.getCurrentTime()).thenReturn(0L);

    assertThrows(OfferNotFoundException.class, () -> offerService.cancelOffer(id));
  }

  @Test
  public void expireOffer() {
    when(mockTimeService.getCurrentTime()).thenReturn(1L);

    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ONE, "foo");

    String id = offerService.submitOffer(offer);

    offerService.retrieveOffer(id);

    offerService.expireOffer(id);

    assertThrows(OfferNotFoundException.class, () -> offerService.retrieveOffer(id));
  }

  @Test
  public void search() {
    when(mockTimeService.getCurrentTime()).thenReturn(1L);

    Offer offer1 = new Offer(0L, Currency.GBP, BigInteger.ONE, "cat");
    Offer offer2 = new Offer(0L, Currency.GBP, BigInteger.ONE, "fish");
    Offer offer3 = new Offer(2L, Currency.GBP, BigInteger.ONE, "bat");

    offerService.submitOffer(offer1);
    offerService.submitOffer(offer2);
    offerService.submitOffer(offer3);

    List<Offer> foundOffers = offerService.searchOffers("at");

    assertThat(foundOffers, hasSize(2));
    assertThat(foundOffers, containsInAnyOrder(offer1, offer3));
  }
}
