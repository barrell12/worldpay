package com.dan.worldpay.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

public class OfferTest {

  @Test
  public void offerHasCurrency() {
    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ZERO, "");
    assertNotNull(offer.getCurrency());
  }

  @Test
  public void offerCannotHaveNullCurrency() {
    assertThrows(NullPointerException.class, () -> new Offer(0L, null, BigInteger.ZERO, ""));
  }

  @Test
  public void offerHasValue() {
    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ZERO, "");
    assertNotNull(offer.getValue());
  }

  @Test
  public void offerCannotHaveNullValue() {
    assertThrows(NullPointerException.class, () -> new Offer(0L, Currency.GBP, null, ""));
  }

  @Test
  public void offerHasDescription() {
    Offer offer = new Offer(0L, Currency.GBP, BigInteger.ZERO, "foo");
    assertNotNull(offer.getDescription());
  }

  @Test
  public void offerCannotHaveNullDescription() {
    assertThrows(NullPointerException.class, () -> new Offer(0L, Currency.GBP, null, null));
  }
}
